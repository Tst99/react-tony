import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from './home/home.component'
import {HelloComponent} from "./hello/hello.component"
import {ListComponent} from "./list/list.component"

// route config
const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'home',
    component:HomeComponent,
    children:[
      {
        path:'list',
        component:ListComponent
      }
    ]
  },
  {
    path:'hello/:id/:name',
    component:HelloComponent
  },
  // common 
  {
    path:"**",
    component:HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
