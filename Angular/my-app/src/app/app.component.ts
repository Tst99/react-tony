import { Component,EventEmitter,Output , OnInit } from '@angular/core';
import { Observable, Subject} from "rxjs";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  constructor(){

  }

  ngOnInit(): void {

    const observable = new Observable((observer)=>{
      let index = 0
      let timer = setInterval(() => {
        observer.next(index++)
        if(index === 3){
          //observer.complete()
          observer.error("error message")
          clearInterval(timer)
        }
      }, 1000)
    })

    const observer = {
      next: (value)=>{
        console.log(value); 
      },
      complete:()=>{
        console.log("completed")
      },
      error:(value)=>{
        console.log(value);
      }
      
    }

    observable.subscribe(observer)

    // Subject

    const demoSubject = new Subject()

    demoSubject.subscribe({
      next: (value)=>{
        console.log(value);
        
      }
    })

    demoSubject.subscribe({
      next: (value)=>{
        console.log(value);
        
      }
    })

    setTimeout(()=>{
      demoSubject.next("hello")
    },2000)

  }
} 


