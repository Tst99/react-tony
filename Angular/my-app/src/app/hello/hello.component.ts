import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  constructor(private B:FormBuilder,private routerInfo:ActivatedRoute) { }

  box:string ="div-dom"
  name:string = "Tina"
  date:Date = new Date()
  dateStr:Date = new Date()
  box2:string ="div-dom"
  itemClass:string = "item-p"
  h3Dom:boolean=true
  h3Class:string = 'h3-dom font-w string'
  isActive:boolean=true
  isMax:boolean=true
  expression:boolean=true
  colors:Array<string>=['red','blue','yellow','green']
  type:number = 2
  title:string=""
  age:FormControl = new FormControl('') 
  loginForm:FormGroup = new FormGroup({
    userName: new FormControl(''),
    password: new FormControl('')
  })
  fromData={
    name:'',
    password:''
  }

  valiDataForm = this.B.group({
    userName:['',[Validators.required,
                  Validators.maxLength(18),
                Validators.minLength(6)]],
    password:['',[this.passWordVal]],
    phone:['',[Validators.required,this.phoneVal]]
  })

  subvaliDataFormFun(){
    console.log(this.valiDataForm.get("password"))
  }

  passWordVal(password:FormControl):object{
    let value = password.value || "";
    if(!value){
      return {msg:"please enter password"}
    }else{
      const valid = value.match(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/);
      return valid?{}:{msg:"password format is wrong"}
    }
  }

  phoneVal(phone:FormControl):object{
    const value = phone.value || "";
    if(!value) return {msg:"enter phone number"}
    const valid = value.match(/[0-9]{11}/);
    return valid? {} : {msg:"contact number should have 11 digits"}
  }

  ngOnInit(): void {
    console.log(this.routerInfo);
    console.log(this.routerInfo.snapshot.queryParams);
    this.routerInfo.params.subscribe((params:Params)=>{
      this.name = params['id']
      console.log(params);
      
    })
  }

  clickFun(e:Event){
    console.log(e)
    alert('You clicked')
  }
  
  inpChange(e:any){
    console.log(e.target.value)
  }
  getUserName(v:string){
    console.log(v)
  }

  ageChangeFun(){
    this.age.setValue(18)
  }

  subFormFun(){
    console.log(this.loginForm.value)
  }

  subBtnFun(obj:any){
    console.log(obj.value)
  }
}
