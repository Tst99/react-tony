import { Component, OnInit, ViewChild } from '@angular/core';

import {ListService} from "../serves/list.service"

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private listService:ListService) { }

  title:string= 'home title'

  @ViewChild('titleDom') child:any;

  // list:Array<string> = ['Angular','React']
  list:Array<string>=[]

  ngOnInit(): void {
    // console.log(this.listService);
    this.list = this.listService.getList()
    console.log(this.list);
  }

  addNode(){
    this.listService.addList("Node")
  }

  addFun(){
    this.child.addBtnFun()
  }

  addListFun(str:string){
    this.list?.push(str)
  }

  page:number = 0;

  pageChange(){
    this.page+=1
  }

  titleChange(){
    this.title="Tina"
  }

}

