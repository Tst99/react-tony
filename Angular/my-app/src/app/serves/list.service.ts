import { Injectable } from '@angular/core';

@Injectable({
  //root import to app.module.ts, null : not to config zone
  providedIn: 'root'
})
export class ListService {

  constructor() { }

  list:Array<string>=["Angular","React","Vue"]

  getList(){
    return this.list
  }

  addList(str:string){
    this.list.push(str)
  }
}
