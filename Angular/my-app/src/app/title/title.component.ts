import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

  constructor() { }

  @Input()
  title?:string
  
  @Output() addList = new EventEmitter()

  addBtnFun(){
    this.addList.emit('Vue')
  }

  ngOnChanges(){
    console.log('ngOnChanges');
  }

  ngOnInit(): void {
    console.log('ngOnInit');
  }
  
  // ngDoCheck(){
  //   console.log('ngDoCheck');
  // }

  // ngAfterContentInit():void{
  //   console.log('ngAfterContentInit');
  // }

  // ngAfterContentChecked():void{
  //   console.log('ngAfterContentChecked');
  // }

  // ngAfterViewInit():void{
  //   console.log('ngAfterViewInit');
  // }

  // ngAfterViewChecked():void{
  //   console.log('ngAfterViewChecked');
  // }

  ngOnDestroy():void{
    console.log('ngOnDestroy');
  }
}
