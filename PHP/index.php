<?php
    // $a = 10;
    // $b = $a;
    // $b = $b + 10;
    // echo $b; // 20
    // echo $a; // 10
//----------------------------
    // $a = 10;
    // $b =&$a;
    // $b = $b + 10;
    // echo $b; // 20
    // echo $a; // 20
//----------------------------
    // $a = "hello";
    // $hello = "world";
    // echo $$a  // world
//----------------------------
    // $a = 10;  // integer
    // $b = 10.0; // double
    // $c = "Tony"; // string
    // $d = true; // boolean
    // $e = ["a","b"]; // array
    // $f = new stdClass ; // object
    // $g = tmpfile(); // resource
    // $h; // NULL
    // echo gettype($h);
    // echo is_int($a);
//----------------------------
    // $a = 22.33; // float
    // settype($a,'integer');
    // echo $a;  // 22
    // settype($a,"float");
    // echo $a; // 22
//----------------------------
    // $a = 22.33;  // float
    // echo (integer)$a; // 22
    // echo (float)$a; //22.33
//----------------------------
    // $a = 10; //global
    // function test(){
    //     global $a;
    //     $b = 20 + $a;
    //     echo $b;
    // }
    // test();
//----------------------------
    // $a = "hello "."world!";
    // echo $a;
//----------------------------
    // $a = 1;
    // $b = 1;
    // $c = "1";
    // $d = 2;
    // echo($a == $b); //1
    // echo($a == $c); //1
    // echo($a == $d); //null
//---------------------------- 
    // $name = [
    //     'first' => 'Peng',
    //     'last'  => 'Jie',
    // ];
    // echo $name['first'];
//---------------------------- 
    // $name = new stdClass();
    // $name->first = 'Peng';
    // $name->last  = 'Jie';
    // $name->first = 'Tony';
    // $name->last  = 'Tam';
    // var_dump($name);
//---------------------------- 
    // class Counter{
    //     public $firstCount = 99;
    // }
    // $countObject = new Counter();    
    // echo $countObject->firstCount //99
//---------------------------- 
    $arr = array();
    $arr['key1']="value1";
    $arr['key2']="value2";
    echo "hello ".$arr['key1'].'<br/>'; // hello value1
    foreach($arr as $key=>$value){
        echo 'key: '.$key.'|value: '.$value."<br/>"; // key: key1|value: value1
                                                     // key: key2|value: value2
    };
//----------------------------         
?>