const http = require("http");

http.createServer((req,res)=>{
    // res.write("hello world")
    // res.end("[1,2,3]")

    res.writeHead(200,{"Content-Type":"text/html;charset=utf-8"})
    res.write(`
        <html>
            <b>Helloooo</b>
            <div>大家好</div>
        </html>
    `)
    res.end()
}).listen(3000,()=>{
    console.log("server start")
})