const http = require("http");
var moduleRenderHTML = require("./module/renderHTML")
var moduleRenderStatus = require("./module/renderStatus")

var server = http.createServer()


server.on("request",(req,res)=>{

    if(req.url === "/favicon.ico"){
        return
    }
    console.log(req.url)
    res.writeHead(moduleRenderStatus.renderStatus(req.url),{"Content-Type":"text/html;charset=utf-8"})
    res.write(moduleRenderHTML.renderHTML(req.url))
    res.end()
})
server.listen(3000,()=>{
    console.log("server start")
})

