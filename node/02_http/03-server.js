//url module

const http = require("http");
var moduleRenderHTML = require("./module/renderHTML")
var moduleRenderStatus = require("./module/renderStatus")
var url = require("url")

var server = http.createServer()


server.on("request",(req,res)=>{

    if(req.url === "/favicon.ico"){
        return
    }
    let pathname = url.parse(req.url).pathname
    var urlobj = url.parse(req.url,true)
    console.log(urlobj)
    console.log(urlobj.query.name,urlobj.query.age)

    // console.log(url.parse(req.url).pathname)
    res.writeHead(moduleRenderStatus.renderStatus(pathname),{"Content-Type":"text/html;charset=utf-8"})
    res.write(moduleRenderHTML.renderHTML(pathname))
    res.end()
})
server.listen(3000,()=>{
    console.log("server start")
})


// const urlString = "https://www.baidu.com:443/ad/index.html?id=8&name=mouse#tag=110"
// const parsedStr = url.parse(urlString)
// console.log(parsedStr)

// const urlObject ={
//     protocol: 'https:',
//     slashes: true,
//     auth: null,
//     host: 'www.baidu.com:443',
//     port: '443',
//     hostname: 'www.baidu.com',
//     hash: '#tag=110',
//     search: '?id=8&name=mouse',
//     query: 'id=8&name=mouse',
//     pathname: '/ad/index.html',
//     path: '/ad/index.html?id=8&name=mouse',
//     href: 'https://www.baidu.com:443/ad/index.html?id=8&name=mouse#tag=110'
//   }

//   console.log(url.format(urlObject))

var a =url.resolve("/one/two/three/","four")
console.log(a)

var b = url.resolve("http://example.com/aaaa/bbbb","/one")
console.log(b)