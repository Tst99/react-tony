const { parse } = require("path");
var querystring = require("querystring")


var str = "name=kerwin&age=100&location=dalian"
var obj = querystring.parse(str)
console.log(obj)
// [Object: null prototype] {
//   name: 'kerwin',
//   age: '100',
//   location: 'dalian'
// }

var myobj = {
    a:1,
    b:2,
    c:3
}
var mystr = querystring.stringify(myobj)
console.log(mystr); // a=1&b=2&c=3

var str1 = "id=3&city=BJ&url=https://www.baidu.com"
var escaped = querystring.escape(str1)
console.log(escaped) // id%3D3%26city%3DBJ%26url%3Dhttps%3A%2F%2Fwww.baidu.com

var escape1 = "id%3D3%26city%3DBJ%26url%3Dhttps%3A%2F%2Fwww.baidu.com"
var str2 = querystring.unescape(escape1)
console.log(str2);  // id=3&city=BJ&url=https://www.baidu.com