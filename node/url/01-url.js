//url module

const http = require("http");
var moduleRenderHTML = require("./module/renderHTML")
var moduleRenderStatus = require("./module/renderStatus")
const {fileURLToPath,urlToHttpOptions} = require("url");
var server = http.createServer()


server.on("request",(req,res)=>{

    if(req.url === "/favicon.ico"){
        return
    }
    // let pathname = url.parse(req.url).pathname
    // var urlobj = url.parse(req.url,true)
    // console.log(urlobj)
    const myURL = new URL(req.url,"https://127.0.0.1:3000/")
    console.log(myURL.searchParams)
    for(var [key,value] of myURL.searchParams){
        console.log(key,value)
    }


    const pathname = myURL.pathname

    res.writeHead(moduleRenderStatus.renderStatus(pathname),{"Content-Type":"text/html;charset=utf-8"})
    res.write(moduleRenderHTML.renderHTML(pathname))
    res.end()
})
server.listen(3000,()=>{
    console.log("server start on 3000")
})

console.log(new URL("file://c://Hello.txt").pathname)
console.log(fileURLToPath("file://c://Hello.txt")); 

const myURL2 = new URL("https://a:b@Test?abc=123#foo")
console.log(myURL2);

console.log(urlToHttpOptions(myURL2)); 
// {
//     protocol: 'https:',
//     hostname: 'test',
//     hash: '#foo',
//     search: '?abc=123',
//     pathname: '/',
//     path: '/?abc=123',
//     href: 'https://a:b@test/?abc=123#foo',
//     auth: 'a:b'
//   }