const {graphql,buildSchema } = require("graphql")
const graphqlHttp = require("express-graphql").graphqlHTTP;
const express = require("express")

const bodyParser = require("body-parser");

var Schema = buildSchema(`
    type Query{
        hello: String
        getName: String,
        getAge: Int
    } 
`)

const root = {
    hello:()=>{
        var str = "hello world"
        return str;
    },
    getName:()=>{
        return "Tony"
    },
    getAge:()=>{
        return 23
    }
}

var app = express()
app.use(bodyParser.json());

app.use("/home",function(req,res){
    res.send("home data")
})
app.use("/list",function (req,res) {
    res.send("list data")
})

app.use("/graphql",graphqlHttp({
    schema:Schema,
    rootValue:root,
    graphiql:true
}))




app.listen(3000,()=>{
    console.log("server started on port 3000");
})

