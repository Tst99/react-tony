const {graphql,buildSchema } = require("graphql")
const graphqlHttp = require("express-graphql").graphqlHTTP;
const express = require("express")

var app = express()

var Schema = buildSchema(`

    type Account{
        name:String,
        age:Int,
        location:String
    }

    type Film{
        id:Int,
        name:String,
        poster:String,
        price:Int
    }

    type Query{
        hello: String
        getName: String,
        getAge: Int,
        getAllNames:[String],
        getAllAges:[Int],
        getAccountInfo: Account,
        getNowplayingList:[Film],
        getFilmDetail(id:Int!):Film
    } 
`)

var fakeDB = [{
    id:1,
    name:"1111",
    poster:"http://1111",
    price:100
},
{
    id:2,
    name:"2222",
    poster:"http://2222",
    price:200
},
{
    id:1,
    name:"3333",
    poster:"http://3333",
    price:300
}
]

const root = {
    hello:()=>{
        var str = "hello world"
        return str;
    },
    getName:()=>{
        return "Tony"
    },
    getAge:()=>{
        return 23
    },
    getAllNames:()=>{
        return ["kerwin","teichui","xiaoming"]
    },
    getAllAges:()=>{
        return [19,20,23]
    },
    getAccountInfo:()=>{
        return{
            name:"kerwin",
            age:100,
            location:"HK"
        }
    },
    getNowplayingList:()=>{
        return fakeDB
    },
    getFilmDetail:({id})=>{
        console.log(id);
        return fakeDB.filter(item=>item.id===id)[0]
    }
}

app.use("/graphql",graphqlHttp({
    schema:Schema,
    rootValue:root,
    graphiql:true
}))


app.listen(3000,()=>{
    console.log("server started on port 3000");
})