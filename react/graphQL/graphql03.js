const {graphql,buildSchema } = require("graphql")
const graphqlHttp = require("express-graphql").graphqlHTTP;
const express = require("express")

var app = express()

var Schema = buildSchema(`

    type Film{
        id:Int,
        name:String,
        poster:String,
        price:Int
    }
    input FilmInput{
        name:String,
        poster:String,
        price:Int
    }

    type Query{
        getNowplayingList:[Film]
    } 

    type Mutation{
        createFilm(input: FilmInput!):Film,
        updateFilm(id:Int!,input:FilmInput):Film
        deleteFilm(id:Int!):Int
    }
`)

var fakeDB = [{
    id:1,
    name:"1111",
    poster:"http://1111",
    price:100
},
{
    id:2,
    name:"2222",
    poster:"http://2222",
    price:200
},
{
    id:3,
    name:"3333",
    poster:"http://3333",
    price:300
}
]

const root = {
    getNowplayingList:()=>{
        return fakeDB
    },

    createFilm:({input})=>{
        var obj = {...input,id:fakeDB.length+1}
        fakeDB.push(obj)
        return obj
    },
    updateFilm({id,input}){
        
        fakeDB.forEach((item,index)=>{
            if(item.id===id){
                current = {...item,...input}
                // console.log(current)
                return fakeDB[index] = current
            }
            return item 
        })
        console.log(fakeDB);
        return current
    },

    deleteFilm({id}){
        fakeDB = fakeDB.filter(item=>item.id!==id)
        return 200
    }

}

app.use("/graphql",graphqlHttp({
    schema:Schema,
    rootValue:root,
    graphiql:true
}))


app.listen(3000,()=>{
    console.log("server started on port 3000");
})