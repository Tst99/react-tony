const {graphql,buildSchema } = require("graphql")
const graphqlHttp = require("express-graphql").graphqlHTTP;
const express = require("express")


var mongoose = require("mongoose")
mongoose.connect("mongodb://127.0.0.1:27017/maizuo",{
    useNewUrlParser: true, useUnifiedTopology:true
})

// limit db films
var FilmModel = mongoose.model("film",new mongoose.Schema({
    name:String,
    poster:String,
    price:Number
}))

// FilmModel.create
// FilmModel.find
// FilmModel.update
// FilmModel.delete

var app = express()

var Schema = buildSchema(`

    type Film{
        id:String,
        name:String,
        poster:String,
        price:Int
    }
    input FilmInput{
        name:String,
        poster:String,
        price:Int
    }

    type Query{
        getNowplayingList(id:String!):[Film]
    } 

    type Mutation{
        createFilm(input: FilmInput!):Film,
        updateFilm(id:String!,input:FilmInput):Film
        deleteFilm(id:String!):Int
    }
`)

var fakeDB = [{
    id:1,
    name:"1111",
    poster:"http://1111",
    price:100
},
{
    id:2,
    name:"2222",
    poster:"http://2222",
    price:200
},
{
    id:3,
    name:"3333",
    poster:"http://3333",
    price:300
}
]

const root = {
    getNowplayingList:({id})=>{
        return FilmModel.find({_id:id})
    },

    createFilm:({input})=>{
        /*
            1. create model
            2. handle db
        */
       return FilmModel.create({
        ...input
       })
    },
    updateFilm({id,input}){
        return FilmModel.updateOne({
            _id:id
        },{
            ...input
        }).then(res=>{
            return FilmModel.find({_id:id})
        }).then(res=>res[0])
    },

    deleteFilm({id}){
        return FilmModel.deleteOne({_id:id}).then(res=>1)
    }

}

app.use("/graphql",graphqlHttp({
    schema:Schema,
    rootValue:root,
    graphiql:true
}))

app.use(express.static("public"))

app.listen(8080,()=>{
    console.log("server started on port 8080");
})