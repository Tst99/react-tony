//1 import React
import React from "react";
import ReactDOM  from "react-dom";

// // !!create

// //2 build react element
// const title = React.createElement('h1',null,'Hello React !!!')
// //3 use react element
// ReactDOM.render(title,document.getElementById('root'))







// //  !!JSX

// //2 use JSX creat react
// const name = 'Jack'
// const age = 19
// const sayHi = () => 'Hi~'
// const dv = <div>I am a div</div>
// const title = (
//     <h1>
//         Hello JSX,{name},age:{age}
//         <span>This is span</span>
//         <p>{1}</p>
//         <p>{'a'}</p>
//         <p>{1+7}</p>
//         <p>{3>5? 'a' : 'b'}</p>
//         {dv}
//     </h1>
// )
// //3 use react
// ReactDOM.render(title,document.getElementById('root'))









// //   !!if-else

// const isLoading = false
// const loadData = () => {
//   if (isLoading){
//     return <div>loading...</div>
//   }
//   return <div>finished loading</div>
// }
// const loadData = () => {
//   return isLoading ? (<div>loading...</div>) : (<div>finished loading!</div>)
// }
// const title = (
//   <h1>
//     condition:
//     {loadData()}
//   </h1>
// )







// // !!maping list

// const songs = [
//     {id: 1, name: 'I Love You'},
//     {id: 2, name: 'Like Me'},
//     {id: 3, name: 'Blue Blue Blue'},
//   ]
//   const title = (
//     <ul>
//       {songs.map(item => <li key={item.id}>{item.name}</li>)}
//     </ul>
//   )  
//   //3 use react
//   ReactDOM.render(title,document.getElementById('root'))







// // !!css

//import "./css/index1.css"
// const title = (
//     <h1 className="title" style = {{ color: 'red', backgroundColor: 'blue' }}>
//       JSX smaple
//     </h1>
//   )
//   //3 use react
// ReactDOM.render(title,document.getElementById('root'))








// // !!Function

// function Hello(){
//     return (
//       <div>This is my first function</div>
//     )
//   }
// //3 use react
// ReactDOM.render(<Hello />,document.getElementById('root'))









// // !!class component

// class Hello extends React.Component {
//     render() {
//       return(
//         <div>This is my First Class Component</div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<Hello />,document.getElementById('root'))








// // !!import

// import Hello from './Hello'
// //3 use react
// ReactDOM.render(<Hello />,document.getElementById('root'))










// // !!button

// class App extends React.Component {
//     // event
//     handleClick() {
//       console.log('event triggered')
//     }
//     render() {
//       return (
//         <button onClick={this.handleClick}>click me Please!!!</button>
//       )
//     }
//   }
//   function App(){
//     function handleClick() {
//       console.log('event triggered')
//     }
//     return (
//       <button onClick={handleClick}>click Me</button>
//     )
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))









//  // !! preventDefault

// class App extends React.Component {
//     handleClick(e){
//       e.preventDefault()
//       console.log('logging')
//     }
//     render(){
//       return (
//         <a href="http://itcast.cn" onClick={this.handleClick}>come on click me</a>
//       )
//     }
//   }
// //3 use react
// ReactDOM.render(<App/>,document.getElementById('root'))









// // //  !! setState({})

// class App extends React.Component {
//     state = {
//       count: 0,
//       test: 'a'
//     }
//     render(){
//       return (
//         <div>
//           <h1> timer : {this.state.count}</h1>
//           <button onClick={ () => {
//             this.setState({
//               count: this.state.count + 1
//             })
//           }}>+1</button>
//         </div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))








// // !!OnIncrement

// class App extends React.Component {
//     state = {
//       count: 0,
//     }
//     onIncrement(){
//       console.log('event : ' , this)
//       this.setState({
//         count: this.state.count + 1
//       })
//     }
//     render(){
//       return (
//         <div>
//           <h1> timer : {this.state.count}</h1>
//           <button onClick={() => this.onIncrement()}>+1</button>
//         </div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))









// // !! bind and  = () => {} 

// class App extends React.Component {
//     // constructor() {
//     //   super()
//     //   this.state = {
//     //     count: 0
//     //   }
//     //   this.onIncrement = this.onIncrement.bind(this)
//     // }
//     state = {
//       count: 0
//     }
//     onIncrement = () => {
//       console.log('event : ' , this)
//       this.setState({
//         count: this.state.count + 1
//       })
//     }
//     render(){
//       return (
//         <div>
//           <h1> timer : {this.state.count}</h1>
//           <button onClick={this.onIncrement}>+1</button>
//         </div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))







// // !!  Changed

// class App extends React.Component {
//     state = {
//       txt: '',
//       content: '',
//       city: 'sh',
//       isChecked: false
//     }
//     handleChange = e => {
//       this.setState({
//         txt: e.target.value 
//       })
//     }
//     handleContent = e => {
//       this.setState({
//         content: e.target.value 
//       })
//     }
//     handleCity = e => {
//       this.setState({
//         city: e.target.value
//       })
//     }
//     handleChecked = e => {
//       this.setState({
//         isChecked: e.target.checked
//       })
//     }
//     render(){
//       return (
//         <div>
//           {/*   Text   */}
//           <input type = "text" value={this.state.txt} onChange = {this.handleChange} />
//           <br/>
//           {/*   Textarea   */}
//           <textarea value = {this.state.content} onChange ={this.handleContent}></textarea>
//           <br/>
//           {/*   Select Bar   */}
//           <select value = {this.state.city} onChange ={this.handleCity}>
//             <option value="hk">Hong Kong</option>
//             <option value="sh">Shang Hai</option>
//             <option value="bj">Bei Jing</option>
//           </select>
//           <br/>
//           {/*   confirm box   */}
//           <input type="checkbox" checked={this.state.isChecked} onChange={this.handleChecked}/>
//         </div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))








// // !! advanced Change

// class App extends React.Component {
//     state = {
//       txt: '',
//       content: '',
//       city: 'sh',
//       isChecked: false
//     }
//     handleForm = e => {
//       // get DOM
//       const target = e.target
//       // check value
//       const value = target.type === 'checkbox'
//         ? target.checked
//         : target.value
//       // get name
//       const name = target.name
//       this.setState({
//         [name] : value
//       })
//     }
//     render(){
//       return (
//         <div>
//           {/*   Text   */}
//           <input type = "text" name="txt" value={this.state.txt} onChange = {this.handleForm} />
//           <br/>
//           {/*   Textarea   */}
//           <textarea value = {this.state.content} name="content" onChange ={this.handleForm}></textarea>
//           <br/>
//           {/*   Select Bar   */}
//           <select name="city" value = {this.state.city} onChange ={this.handleForm}>
//             <option value="hk">Hong Kong</option>
//             <option value="sh">Shang Hai</option>
//             <option value="bj">Bei Jing</option>
//           </select>
//           <br/>
//           {/*   confirm box   */}
//           <input type="checkbox" name="isChecked"  checked={this.state.isChecked} onChange={this.handleForm}/>
//         </div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))









// // !! REF

// class App extends React.Component {
//     constructor() {
//       super()
//       //create ref
//       this.txtRef = React.createRef()
//     }
//     // get text value
//     getTxt = () =>{
//       console.log('txt value : ',this.txtRef.current.value);
//     }
//     render(){
//       return (
//         <div>
//           <input type="text" ref={this.txtRef}/>
//           <button onClick={this.getTxt}>get value</button>
//         </div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))











// // renderList

// class App extends React.Component {
//     // initialize state
//     state = {
//       comments :[
//         {id:1 , name:'jack', content: 'sofaaaaa!'},
//         {id:2 , name:'rose', content: 'chair!'},
//         {id:3 , name:'tom' , content: 'Thank You!'}
//       ]
//     }
//     // render comment list:
//     renderList() {
//       const {comments} = this.state
//       return comments.length === 0 ?(<div className="no-comment">no comment</div>)
//       :(
//       <ul>
//         {comments.map(item => (
//           <li key = {item.id}>
//             <h3>Name: {item.name}</h3>
//             <p>Comment: {item.content}</p>
//           </li>
//         ))}
//       </ul>
//       )
//     }
//     // render list
//     render(){
//       return (
//         <div className="app">
//           <div>
//             <input className="user" type="text" placeholder="who are you" />
//             <br />
//             <textarea
//               className="content"
//               cols="30"
//               rows="10"
//               placeholder="type comment"
//             ></textarea>
//             <br></br>
//             <button>comment</button>
//           </div>
//           {/* condition */}
//           {this.renderList()}
//           {/* {{this.state.comments.length === 0 ?(<div className="no-comment">no comment</div>)
//             :(
//             <ul>
//               {this.state.comments.map(item => (
//                 <li key = {item.id}>
//                   <h3>Name: {item.name}</h3>
//                   <p>comment: {item.content}</p>
//                 </li>
//               ))}
//             </ul>
//         )} } */}
//         </div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))













//  !!!!!  Whole Comment

// class App extends React.Component {
//     // initialize state
//     state = {
//       comments :[
//         {id:1 , name:'jack', content: 'sofaaaaa!'},
//         {id:2 , name:'rose', content: 'chair!'},
//         {id:3 , name:'tom' , content: 'Thank You!'}
//       ],
//       userName: '',
//       userContent: ''
//     }
//     // render comment list:
//     renderList() {
//       const {comments} = this.state
//       return comments.length === 0 ?(<div className="no-comment">no comment</div>)
//       :(
//       <ul>
//         {comments.map(item => (
//           <li key = {item.id}>
//             <h3>Name: {item.name}</h3>
//             <p>Comment: {item.content}</p>
//           </li>
//         ))}
//       </ul>
//       )
//     }
//     // handle Form
//     handleForm = (e)=>{
//       const{name,value} =e.target
//       this.setState({
//         [name]:value
//       })
//     }
//     // Add comment:
//     addComment = () => {
//       const{comments, userName,userContent} = this.state
//       // check null
//       if (userName.trim() === '' || userContent.trim() === ''){
//         alert('please name and content')
//         return
//       }
//       // add content to state
//       const newComments = [{
//         id: Math.random(),
//         name: userName,
//         content: userContent
//       }, 
//       ...comments]
//       //  clear value and post
//       this.setState({
//         comments: newComments,
//         userName: '',
//         userContent: ''
//       })
//     }
//     // render list
//     render(){
//       const {userName, userContent} =  this.state
//       return (
//         <div className="app">
//           <div>
//             <input className="user" type="text" placeholder="who are you" value={userName} name="userName" onChange={this.handleForm}/>
//             <br />
//             <textarea
//               className="content"
//               cols="30"
//               rows="10"
//               placeholder="type comment"
//               value={userContent}
//               name="userContent"
//               onChange={this.handleForm}
//             ></textarea>
//             <br></br>
//             <button onClick={this.addComment}>comment</button>
//           </div>
//           {this.renderList()}
//         </div>
//       )
//     }
//   }
//   //3 use react
//   ReactDOM.render(<App/>,document.getElementById('root'))













// // !!! Props

// 2 get data from props (read only)
// class Hello extends React.Component {
//     constructor(props){
//       super(props)
//       console.log(this.props)
//     }
//     render(){
//       console.log(this.props)
//       return(
//       <div>
//         <h1>props:{this.props.age}</h1>
//       </div>
//       )
//     }
//   }
//   //3 send data
//   ReactDOM.render(<Hello name="jack" age={19} colors={['red','green','blue']} fn={() => console.log('this is a function')}/>,document.getElementById('root'))








// // !! father to child

// class Parent extends React.Component {
//     state = {
//       lastName: 'Wong'
//     }
//     render(){
//       return (
//         <div className="parent">
//           Father:
//           <Child name={this.state.lastName}/>
//         </div>
//       )
//     }
//   }
//   const Child = props => {
//     console.log('props: ',props)
//     return(
//       <div className="child">
//        child:  <button>click me! send data to father</button>
//       </div>
//     )
//   }
//   //3 send data
//   ReactDOM.render(<Parent/> ,document.getElementById('root'))










//  //  !!! child to father

// class Parent extends React.Component {
//     state = {
//       parentMsg: ''
//     }
//     // provide function
//     getChildMsg = (data) => {
//       console.log('get data from child: ', data)
//       this.setState({
//         parentMsg : data
//       })
//     }
//     render(){
//       return (
//         <div className="parent">
//           father: {this.state.parentMsg}
//           <Child getMsg={this.getChildMsg}/>
//         </div>
//       )
//     }
//   }
//   // Child 
//   class Child extends React.Component{
//     state = {
//       msg:'Tik Tok'
//     }
//     handleClick = () => {
//       // child call father function
//       this.props.getMsg(this.state.msg)
//     }
//     render(){
//       return(
//         <div className="child">
//           child: <button onClick={this.handleClick}>click me, send data to father </button>
//         </div>
//       )
//     }
//   }
//   //3 send data
//   ReactDOM.render(<Parent />, document.getElementById('root'))









// // !! brother to brother

// // father
// class Counter extends React.Component {
//     // provide share state
//     state = {
//       count: 0
//     }
//     // provide ways to change state
//     onIncrement = () => {
//       this.setState({
//         count: this.state.count + 1
//       })
//     }
//     render(){
//       return(
//         <div>
//           <Child1 count={this.state.count}/>
//           <Child2 change={this.onIncrement}/>
//         </div>
//       )
//     }
//   }
//   const Child1 = props => {
//     return <h1>counter : {props.count}</h1>
//   }
//   const Child2 = props => {
//     return <button onClick={() => props.change()}>+1</button>
//   }
//   //3 send data
//   ReactDOM.render(<Counter />, document.getElementById('root'))











//  Context for longdistance data transmit

// // create context
// const {Provider,Consumer} = React.createContext()
// class App extends React.Component {
//   render(){
//     return(
//       <Provider value="pink">
//         <div className="app">
//           <Node />
//         </div>
//       </Provider>
//     )
//   }
// }
// const Node = props => {
//   return (
//     <div className="node">
//       <SubNode />
//     </div>
//   )
// }
// const SubNode = props => {
//   return (
//     <div className="subnode">
//       <Child/>
//     </div>
//   )
// }
// const Child = props => {
//   return <div className="child">
//     <Consumer>
//       {          
//         data => <span>I am subnode -- {data}</span>
//       }
//     </Consumer>
//   </div>
// }
// //3 send data
// ReactDOM.render(<App />, document.getElementById('root'))










// call children

// const Test = () => <button>I am button</button>
// const App = props => {
//   console.log(props)
//   props.children()
//   return (
//     <div>
//       <h1>child node: </h1>
//       {props.children}
//     </div>
//   )
// }
// //3 send data
// ReactDOM.render(
//   <App>
//     {/* <p>I am child node</p>
//     <Test/> */}
//     {
//       () => console.log('this is a function node')
//     }
//   </App>,document.getElementById('root'))












// // !!propsType

// //import PropTypes
// import PropTypes from 'prop-types'
// const App = props => {
//   const arr = props.colors
//   const list = arr.map((item,index) => <li key = {index}>{item}</li>)
//   return <ul>{list}</ul>
// }
// //add propTypes
// App.propTypes= {
//   color: PropTypes.array
// }
// //3 send data
// ReactDOM.render(<App colors={['red','blue']}/>,document.getElementById('root'))







// // !!propsType

// import PropTypes from 'prop-types'
// const App = props => {
//   return (
//     <div>
//       <h1>props check:</h1>
//     </div>
//   )
// }
// //add propTypes
// App.propTypes= {
//   a: PropTypes.number,
//   fn: PropTypes.func.isRequired,
//   tag: PropTypes.element,
//   filter: PropTypes.shape({
//     area: PropTypes.string,
//     price: PropTypes.number
//   })
// }
// //3 send data
// ReactDOM.render(<App fn = {() => {}}/>,document.getElementById('root'))








// // !! default props

// const App = props => {
//     console.log(props)
//     return (
//       <div>
//         <h1>Here is the props's default value:{props.pageSize}</h1>
//       </div>
//     )
//   }
//   // add props default
//   App.defaultProps = {
//     pageSize: 10
//   }
// //3 send data
// ReactDOM.render(<App pageSize={20}/>,document.getElementById('root'))











// // !!  Life Cycle

// class App extends React.Component{
//     constructor(props){
//       super(props)
//       this.state = {
//         count : 0
//       }
//       console.warn('Life Cycle: constructor')
//     }
//     // run DOM
//     // send AJAX
//     componentDidMount(){
//       const title = document.getElementById('title')
//       console.log(title)
//       console.warn('Life Cycle: componentDidMount')
//     }
//     render() {
//       console.warn('Life Cycle: render')
//       return(
//         <div>
//           <h1 id="title"> Sum of hit: {this.state.count}</h1>
//           <button id="btn">hit</button>
//         </div>
//       )
//     }
//   }
//   //3 send data
//   ReactDOM.render(<App />,document.getElementById('root'))









//  // ! Render Timing ( forceUpdate()  ,  New Props    ,   setState)

// class App extends React.Component{
//     constructor(props){
//       super(props)
//       this.state = {
//         count : 0
//       }
//     }
//     // hit
//     handleClick = () => {
//       this.setState({
//         count: this.state.count + 1
//       })
//       // this.forceUpdate()
//     }
//     render() {
//       console.warn('life cycle : render')
//       return(
//         <div>
//           <Counter count = {this.state.count}/>
//           <button onClick={this.handleClick}>hit</button>
//         </div>
//       )
//     }
//   }
//   class Counter extends React.Component {
//     render() {
//       console.warn('Child life cycle : render')
//       return <h1>sum of hit: {this.props.count}</h1>
//     }
//   }
//   //3 send data
//   ReactDOM.render(<App />,document.getElementById('root'))










// // !! componentDidUpdate

// class App extends React.Component{
//     constructor(props){
//       super(props)
//       this.state = {
//         count : 0
//       }
//     }
//     // hit
//     handleClick = () => {
//       this.setState({
//         count: this.state.count + 1
//       })
//       // this.forceUpdate()
//     }
//     render() {
//       return(
//         <div>
//           <Counter count = {this.state.count}/>
//           <button onClick={this.handleClick}>hit</button>
//         </div>
//       )
//     }
//   }
//   class Counter extends React.Component {
//     render() {
//       console.warn('Child life cycle : render')
//       return <h1 id = "title">sum of hit: {this.props.count}</h1>
//     }
//     componentDidUpdate(prevProps){
//       console.warn('Child life cycle : componentDidUpdate')
//       console.log('Prev: ', prevProps, '  Latest : ',this.props)
//       if (prevProps.count !== this.props.count){
//         this.setState({})
//         // send Ajax
//       }
//       // get DOM
//       const title = document.getElementById('title')
//       console.log(title.innerHTML)
//     }
//   }
//   //3 send data
//   ReactDOM.render(<App />,document.getElementById('root'))










// // !!  conponentWillUnmount

// class App extends React.Component{
//     constructor(props){
//       super(props)
//       this.state = {
//         count : 0
//       }
//     }
//     // hit
//     handleClick = () => {
//       this.setState({
//         count: this.state.count + 1
//       })
//       // this.forceUpdate()
//     }
//     render() {
//       return(
//         <div>
//           {this.state.count > 3 ?(
//              <p>I am dead</p>)
//             : (
//               <Counter count = {this.state.count}/>
//             )}
//           <button onClick={this.handleClick}>hit</button>
//         </div>
//       )
//     }
//   }
//   class Counter extends React.Component {
//     componentDidMount(){
//       this.timerId = setInterval(() => {
//         console.log('timer is running')
//       }, 1000)
//     }
//     render() {
//       console.warn('Child life cycle : render')
//       return <h1 id = "title">sum of hit: {this.props.count}</h1>
//     }
//     componentWillUnmount(){
//       console.warn('Life Cycle Function:  componentWillUnmount')
//       clearInterval(this.timerId)
//     }
//   }
//   //3 send data
//   ReactDOM.render(<App />,document.getElementById('root'))










// //  !! track mouse position

// class Mouse extends React.Component{
//     state = {
//       x:0,
//       y:0
//     }
//     handleMouseMove = e => {
//       this.setState({
//         x: e.clientX,
//         y: e.clientY
//       })
//     }
//     componentDidMount(){
//       window.addEventListener('mousemove', this.handleMouseMove)
//     }
//     render(){
//       return this.props.renders(this.state)
//     }
//   }
//   class App extends React.Component{
//     render(){
//       return (
//         <div>
//           <h1>render props mode</h1>
//           <Mouse renders={(mouse) => {
//             return <p>Mouse position: {mouse.x} {mouse.y}</p>
//           }}/>
//         </div>
//       )
//     }
//   }
//   //3 send data
//   ReactDOM.render(<App />,document.getElementById('root'))









// // !! ADD  Cat  to  mouse

// import img from './cat.png'
// class Mouse extends React.Component{
//   state = {
//     x:0,
//     y:0
//   }
//   handleMouseMove = e => {
//     this.setState({
//       x: e.clientX,
//       y: e.clientY
//     })
//   }
//   componentDidMount(){
//     window.addEventListener('mousemove', this.handleMouseMove)
//   }
//   componentWillUnmount(){
//     window.removeEventListener('mousemove', this.handleMouseMove)
//   }
//   // render(){
//   //   return this.props.renders(this.state)
//   // }
//   render(){
//     return this.props.children(this.state)
//   }
// }
// Mouse.protoTypes ={
//   children: PropTypes.func.isRequired
// }
// class App extends React.Component{
//   render(){
//     return (
//       <div>
//         <h1>render props mode</h1>
//         {/* <Mouse renders={(mouse) => {
//           return <p>Mouse position: {mouse.x} {mouse.y}</p>
//         }}/> */}
//         <Mouse>
//           {
//             mouse => {
//               return (
//                 <p>
//                   Mouse Position: {mouse.x} {mouse.y}
//                 </p>
//               )
//             }
//           }
//         </Mouse>
//         {/* Cat */}
//         <Mouse>
//           {mouse => (
//             <img src={img} alt="cat" style={{
//               height: 128,
//               width : 128,
//               position : 'absolute',
//               top: mouse.y -64,
//               left: mouse.x -64
//             }}
//           />
//           )}
//         </Mouse>
//         {/* <Mouse renders = {mouse => {
//           return <img src={img} alt="cat" style={{
//             height: 128,
//             width : 128,
//             position : 'absolute',
//             top: mouse.y -64,
//             left: mouse.x -64
//           }}/>
//         }}/> */}
//       </div>
//     )
//   }
// }
// //3 send data
// ReactDOM.render(<App />,document.getElementById('root'))









// // !!!!! High Level component

// function withMouse(WrappedComponent){
//     console.log(WrappedComponent.name)
//     class Mouse extends React.Component{
//       state = {
//         x:0,
//         y:0
//       }
//       handleMouseMove = e => {
//         this.setState({
//           x: e.clientX,
//           y: e.clientY
//         })
//       }
//       componentDidMount(){window.addEventListener('mousemove', this.handleMouseMove)}
//       componentWillUnmount(){window.removeEventListener('mousemove', this.handleMouseMove)}
//       render(){
//         return <WrappedComponent {...this.state} {...this.props}></WrappedComponent>
//       }
//     }
//     Mouse.displayName = `WithMouse ${getDisplayName(WrappedComponent)}`
//     return Mouse
//   }
//   function getDisplayName(WrappedComponent){
//     return WrappedComponent.displayName || WrappedComponent.name || 'Component'
//   }
//   // Test high level props
//   const Position = props => (
//     <p>
//       Mouse position: (x: {props.x},y:{props.y})
//     </p>
//   )
//   // Cat props
//   const Cat = props =>(
//     <img src={img} alt="" style={{
//       position: "absolute",
//       top: props.y -100,
//       left: props.x -90,
//       height: 128,
//       width: 128
//     }}/>
//   )
//   //  Get buffed Props
//   const MousePosition = withMouse(Position)
//   const MouseCat = withMouse(Cat)
//   class App extends React.Component{
//     render(){
//       return(
//         <div>
//           <h1>High level module</h1>
//           <MousePosition a="1"/>
//           <MouseCat/>
//         </div>
//       )
//     }
//   }
//   //3 send data
//   ReactDOM.render(<App />,document.getElementById('root'))











// // recommand setState  and second function

// class App extends React.Component {
//     state = {
//       count : 1
//     }
//     handleClick = () => {
//     //   this.setState({
//     //     count : this.state.count + 1
//     //   })
//     //   console.log('count: ',this.state.count)
//     //   this.setState({
//     //     count: this.state.count + 2
//     //   })
//     //   console.log('count: ',this.state.count)
//       this.setState((state,props) => {
//         return{
//           count: state.count + 1
//         }
//       })
//       this.setState((state,props) => {
//         return{
//           count: state.count + 1
//         },
//         // second function
//         () => {
//             console.log('State Updated: ', this.state.count)
//         }
//       })
//       console.log('count',this.state.count)//1
//     }
//     render() {
//       return (
//         <div>
//           <h1>Counter: {this.state.count}</h1>
//           <button onClick={this.handleClick}>+1</button>
//         </div>
//       )
//     }
//   }
//   //3 send data
//   ReactDOM.render(<App />,document.getElementById('root'))












//  // shallow compare

// class App extends React.PureComponent{
//     state = {
//         obj:{
//           number : 0 , 
//           number2 : 2
//         }
//     }
//     handleClick = () => {
//         const newObj = {...this.state.obj, number: Math.floor(Math.random()*3)}
//         console.log(newObj)
//         this.setState(() => {
//             return {
//                 obj: newObj
//             }
//         })
//         // Wrong
//         // const newObj = this.state.obj
//         // newObj.number = Math.floor(Math.random()*3)
//         // this.setState(() => {
//         //     return {
//         //         obj: newObj
//         //     }
//         // })
//     }
//     render(){
//         return (
//             <div>
//                 <NumberBox number = {this.state.obj.number}/>
//                 {/* <h1>random number: {this.state.number}</h1> */}
//                 <button onClick={this.handleClick}>click</button>
//             </div>
//         )
//     }
// }
// class NumberBox extends React.PureComponent{
//     render(){
//         console.log('child render')
//         return <h1>Random: {this.props.number}</h1>
//     }
// }
// //3 send data
// ReactDOM.render(<App></App>, document.getElementById('root'))










// //Routers

//1 import React
// import React from "react";
// import ReactDOM from "react-dom/client";
// import {BrowserRouter as Router,Route, Link, Routes} from 'react-router-dom'
// const First = () => (<p>Page 1 content</p>)
// const Topic = () => (
//     <div>
//         <p>Topic</p>
//         <Link to="/first">Page 1</Link>
//     </div>
// )
// const Routers = () => (
//     <div>
//         <Router>
//             <Routes>
//                 <Route path="/" element={<Topic/>}/>
//                 <Route path="first" element={<First/>}/>
//             </Routes>
//         </Router>
//     </div>
// )
// //3 send data
// const root = ReactDOM.createRoot(document.getElementById("root"));
// root.render(<Routers/>)














// Navigate

// import { BrowserRouter as Router, Route, Link, Routes,useNavigate} from 'react-router-dom';
// import {withRouter} from './withRouter';
// class SecondRaw extends React.Component{
//     constructor(){
//         super()
//         this.handleClick=this.handleClick.bind(this);
//     }
//     handleClick = () =>{
//         console.log(this.props)
//         this.props.navigate('/first')
//     }
//     render(){
//         return (
//             <div>
//                 <p>Page 2</p>
//                 <button onClick={this.handleClick}>Page 1</button>
//                 <Link to="/first">Page 1</Link>
//             </div>
//         )
//     }
// }
// const Second = withRouter(SecondRaw)
// function First() {
//     const navigate = useNavigate();
//     return (
//         <div>
//             <p>Page 1</p>
//             <button onClick={() => navigate('/second')}>Page 2</button>
//             <button onClick={() => navigate('hide')}>Hide page</button>
//         </div>
//     );
//   }
// class Topic extends React.Component {
//     render() {
//         return (
//             <div>
//                 <p>Topic</p>
//                 <Link to="/first">Page 1</Link>
//             </div>
//         )
//     }
// }
// class Hide extends React.Component{
//     render(){
//         return(
//             <div>
//                 <p>Hide page</p>
//                 <Link to="/">Home</Link>
//             </div>
//         )
//     }
// }
// const Routers = () => (
//     <div>
//         <Router>
//             <Routes>
//                 <Route path="/" element={<Topic />} />
//                 <Route path="/first" element={<First />} />
//                 <Route path="/second" element={<Second />} />
//                 <Route path="/first/hide" element={<Hide/>}/>
//             </Routes>
//         </Router>
//     </div>
// )
// //3 send data
// const root = ReactDOM.createRoot(document.getElementById("root"));
// root.render(<Routers />)