import React from 'react'
import{ApolloProvider, Query} from "react-apollo"
import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'



const client = new ApolloClient({
    uri:"/graphql"
})


const KerwinQuery = () =>{
    var query = gql`
        query{
            getNowplayingList {
                id,
                name,
                poster,
                price
            }
        }
    `

    return(
        <Query query={query}>
            {
                ({loading,data})=>{
                    console.log(data)
                    return loading?<div>loading</div>:
                    <div>
                        {
                            data.getNowplayingList.map(item=>
                                <div key={item.id}>
                                    <div>{item.name}</div>
                                    <div>{item.price}</div>
                                </div>
                            )
                        }
                    </div>
                }
            }
        </Query>
    )
}


function App() {
  return (
    <ApolloProvider client={client}>
        <div>
            <KerwinQuery></KerwinQuery>
        </div>
    </ApolloProvider>
  )
}

export default App

