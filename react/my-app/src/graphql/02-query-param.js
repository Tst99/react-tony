import React, { useState } from 'react'
import { ApolloProvider, Query } from "react-apollo"
import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'



const client = new ApolloClient({
    uri: "/graphql"
})


const KerwinQuery = () => {
    var [state,setstate] = useState({id:"62d6e489cc0ce46acfcaeecf"})

    var query = gql`
        query getNowplayingList($id:String!){
            getNowplayingList(id:$id) {
                id,
                name,
                poster,
                price
            }
        }
    `

    return (
        <div>
            <input  type="text" onChange={(evt)=>{
                setstate({
                    id:evt.target.value
                })
            }}/>
            <Query query={query} variables={{id:state.id}}>
                {
                    ({ loading, data }) => {
                        console.log(data)
                        return loading ? <div>loading</div> :
                            <div>
                                {
                                    data.getNowplayingList.map(item =>
                                        <div key={item.id}>
                                            <div>{item.name}</div>
                                            <div>{item.price}</div>
                                        </div>
                                    )
                                }
                            </div>
                    }
                }
            </Query>
        </div>
    )
}


function App() {
    return (
        <ApolloProvider client={client}>
            <div>
                <KerwinQuery></KerwinQuery>
            </div>
        </ApolloProvider>
    )
}

export default App

