import React, { useState } from 'react'
import { ApolloProvider, Mutation } from "react-apollo"
import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'



const client = new ApolloClient({
    uri: "/graphql"
})


const KerwinQuery = () => {

    var createFilm = gql`
    mutation createFilm($input:FilmInput!){
        createFilm(input:$input){
            id,
            name,
            price
        }
    }
    `

    return (
        <div>
            <Mutation mutation={ createFilm }>
                {
                    (create,{data})=>{
                        console.log(data)
                        console.log(create)
                        return <div>
                            <button onClick={()=>{
                                create({
                                    variables:{
                                        input:{
                                            name:"777",
                                            poster:"http://777",
                                            price:70
                                        }
                                    }
                                })
                            }}>add</button>
                        </div>
                    }
                }
            </Mutation>
        </div>
    )
}


function App() {
    return (
        <ApolloProvider client={client}>
            <div>
                <KerwinQuery></KerwinQuery>
            </div>
        </ApolloProvider>
    )
}

export default App

