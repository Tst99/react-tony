import React, { useState } from 'react'
import { ApolloProvider, Mutation } from "react-apollo"
import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'




const client = new ApolloClient({
    uri: "/graphql"
})


const KerwinQuery = () => {

    var updateFilm = gql`
    mutation updateFilm($id:String!,$input:FilmInput){
        updateFilm(id:$id,input:$input){
            id,
            name,
            price
        }
    }
    `

    return (
        <div>
            <Mutation mutation={ updateFilm }>
                {
                    (update,{data})=>{
                        console.log(data)
                        return <div>
                            <button onClick={()=>{
                                update({
                                    variables:{
                                        id:"62d8323e7a4b1a792dc871ed",
                                        input:{
                                            name:"777777",
                                            poster:"http://77777777",
                                            price:700
                                        }
                                    }
                                })
                            }}>update</button>
                        </div>
                    }
                }
            </Mutation>
        </div>
    )
}


function App() {
    return (
        <ApolloProvider client={client}>
            <div>
                <KerwinQuery></KerwinQuery>
            </div>
        </ApolloProvider>
    )
}

export default App

