import React, { useState } from 'react'
import { ApolloProvider, Mutation } from "react-apollo"
import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'




const client = new ApolloClient({
    uri: "/graphql"
})


const KerwinQuery = () => {

    var deleteFilm = gql`
    mutation deleteFilm($id:String!){
        deleteFilm(id:$id)
    }
    `

    return (
        <div>
            <Mutation mutation={ deleteFilm }>
                {
                    (deleteFilm,{data})=>{
                        console.log(data)
                        return <div>
                            <button onClick={()=>{
                                deleteFilm({
                                    variables:{
                                        id:"62d8323e7a4b1a792dc871ed"
                                    }
                                })
                            }}>delete</button>
                        </div>
                    }
                }
            </Mutation>
        </div>
    )
}


function App() {
    return (
        <ApolloProvider client={client}>
            <div>
                <KerwinQuery></KerwinQuery>
            </div>
        </ApolloProvider>
    )
}

export default App
