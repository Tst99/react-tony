import React ,{useState}from "react";
import  ReactDOM  from "react-dom";
import App2 from "./CustomHooks";
import Cheer from "./Custom2"

const App=()=>{
    const [value,setValue]=useState(0);
    const [score,setScore]=useState(0);
    return(
          <div id="App">
            <App2 value={value} onClick={(e)=>{setValue(e.target.value)}}/>
            <Cheer value={score} onClick={(e)=>{setScore(e.target.value)}}/>
          </div>
          )
    }

ReactDOM.render(
    <App/>,document.getElementById("root")
)