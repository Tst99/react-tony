import React, { useEffect, useState } from 'react'
import { observable, autorun, toJS } from 'mobx'
import { inject, observer, useLocalObservable } from "mobx-react"
import store from './store/store'


// var observableNumber = observable.box(10)
// var observableName = observable.box("Kerwin")

// autorun(()=>{
//     console.log(observableNumber.get())
// })

// autorun(()=>{
//     console.log(observableName.get())
// })

// setTimeout(()=>{
//   observableNumber.set(20)  
// },1000)

// setTimeout(()=>{
//     observableName.set("Tony")  
//   },2000)

// var myobj = observable({
//     name:"kerwin",
//     age:100
// })

// autorun(()=>{
//     console.log("obj changed",myobj.name)
// })

// setTimeout(()=>{
//     // myobj.set("name","Koby")
//     myobj.name = "Koby"
//   },3000)

const App  = inject("store")(observer(({store}) => {
    return (
        <div>
            {store.Name}
            <br></br>
            {store.Gender}
            <br></br>
            {toJS(store.Role.roleName)}
            <br></br>
            <button onClick={()=>{
                store.changeName()
            }}>changeName</button>
            <button onClick={()=>{
                store.changeGender()
            }}>changeGender</button>
            <button onClick={async()=>{
                await store.getRole()
                // await console.log(toJS(store.Role))
            }}>Console Roles</button>
        </div>
    )
}))



export default App