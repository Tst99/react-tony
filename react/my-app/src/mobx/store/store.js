import axios from "axios";
import { autorun, observable, configure, action ,makeObservable,flow,computed ,toJS} from "mobx";

// const store = observable({
//     Name: "Tony",
//     Age: 23,
//     Gender: "Male",
// changeName() {
//     if (this.Name == "Tony") {
//         this.Name = "Billy"
//     } else {
//         this.Name = "Tony"
//     }
// },
// changeGender() {
//     if (this.Gender == "Male") {
//         this.Gender = "Female"
//     } else {
//         this.Gender = "Male"
//     }
// }
// }, {
//     changeGender: action,   // mark action
//     changeName: action
// })
configure({
    enforceActions: "always"
})

class Store {
    Name = "Tony"
    Age = 23
    Gender = "Male"
    Role = {}

    constructor(){
        makeObservable(this,{
            Name: observable,
            Age: observable,
            Gender: observable,
            Role: observable,
            getRole: flow,
            changeName: action,
            changeGender: action,
            changeRole: action
        })
    }

    *getRole(){
        axios.get("https://donutbaba.shop/roles").then(res =>{
            this.changeRole(res.data[0])
            console.log(res.data[0])
        })
        // console.log(toJS(this.Role))
    }

    changeRole(value){
        this.Role = value
    }

    changeGender() {
        if (this.Gender == "Male") {
            this.Gender = "Female"
        } else {
            this.Gender = "Male"
        }
    }
    changeName() {
        if (this.Name == "Tony") {
            this.Name = "Billy"
        } else {
            this.Name = "Tony"
        }
    }
}


const store = new Store()



export default store