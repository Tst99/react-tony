
function *test() {
    console.log('111')
    var input1 = yield "111-output"; // = next value
    console.log("222",input1)
    var input2 = yield "222-output";
    console.log("333",input2);
    var input3 = yield "333-output";
    console.log("444",input3);
}

var kerwintest = test()

var res1 = kerwintest.next("aaaa")  // = yield value
console.log(res1);
var res2 = kerwintest.next("bbbb")
console.log(res2);
var res3 = kerwintest.next("cccc")
console.log(res3);
var res4 = kerwintest.next("dddd")
console.log(res4);

function *test1() {
    setTimeout(()=>{
        console.log("1111-s")
        kerwintest1.next()
    },1000)
    yield;
    setTimeout(()=>{
        console.log("2222-s")
        kerwintest1.next()
    },1000)
    yield;
    setTimeout(()=>{
        console.log("3333-s")

    },1000)
    yield;
}

var kerwintest1 = test1()

kerwintest1.next()


