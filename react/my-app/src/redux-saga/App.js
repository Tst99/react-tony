import React from 'react'
// import "./02-runnable"
import store from "./redux/store"

export default function App() {

  return (
    <div>
      <button onClick={() => {
        if (store.getState().list1.length === 0) {
          store.dispatch({
            type: "get-list1"
          })
        } else {
          console.log("Ram", store.getState().list1)
        }
      }}>click-ajax-1</button>

      <button onClick={() => {
        if (store.getState().list2.length === 0) {
          store.dispatch({
            type: "get-list2"
          })
        } else {
          console.log("Ram", store.getState().list2)
        }
      }}>click-ajax-2</button>
    </div>
  )
}
