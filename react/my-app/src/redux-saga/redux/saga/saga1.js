import {take,fork,put,call, takeEvery} from "redux-saga/effects"

function *watchSaga1() {
    // while(true){
    //     // take listen action from components
    //     yield take("get-list1")
    //     // fork sycn call function
    //     yield fork(getList1)
    // }
    yield takeEvery("get-list1",getList1)
}

function *getList1(){
    // asycn
    // call function for asycn
    let res = yield call(getListAction1) 
    /// put function to send new action
    yield put({
        type:"change-list1",
        payload: res
    })
}

function getListAction1(){
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            resolve(['1111','2222','3333'])
        }, 2000);
    })
}

export default watchSaga1
export {getList1}