import {take,fork,put,call, takeEvery} from "redux-saga/effects"

function *watchSaga2() {
    // while(true){
    //     // take listen action from components
    //     yield take("get-list2")
    //     // fork sycn call function
    //     yield fork(getList2)
    // }
    yield takeEvery("get-list2",getList2)
}

function *getList2(){
    // asycn
    // call function for asycn
    let res1 = yield call(getListAction2_1) 
    let res2 = yield call(getListAction2_2,res1) 
    /// put function to send new action
    yield put({
        type:"change-list2",
        payload: res2
    })
}

function getListAction2_1(){
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            resolve(['4444','5555','6666'])
        }, 2000);
    })
}
function getListAction2_2(data){
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            resolve([...data,"777","888","999"])
        }, 2000);
    })
}

export default watchSaga2
export {getList2}