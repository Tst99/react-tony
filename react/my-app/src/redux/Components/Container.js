import Reacts from "react";
import { addAction, squareAction ,getAction} from "../actions/actions"
import { connect } from "react-redux";

function Container(props) {

    const { num, add, square ,get} = props;

    return (
        <div>
            <div>{num}</div>
            <button onClick={() => {
                add(1)
            }}>ADD ONE</button>

            <button onClick={() => {
                add(2)
            }}>ADD TWO</button>

            <button onClick={() => {
                square()
            }}>SQUARE</button>

            <button onClick={()=>{
                get()
            }}>Reset</button>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        num: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        add: (value) => dispatch(addAction(value)),
        square: () => dispatch(squareAction()),
        get: () => dispatch(getAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container)

