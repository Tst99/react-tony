import {ADD,SQUARE,RESET} from  "../constans/ActionTypes"

const getAction = () =>{
    return(dispatch,getState) =>{

        fetch("/data.json")
            .then(res => res.json())
            .then(res => {
                dispatch({
                    type: RESET,
                    num : res.num
                })
            })
    }
}

const addAction = (num) =>{
    return{
        type: ADD,
        num
    }
}

const squareAction = () =>{
    return{
        type: SQUARE,
    }
}

export{
    addAction,
    squareAction,
    getAction
}