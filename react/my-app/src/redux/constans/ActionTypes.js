const ADD = "ADD";
const SQUARE = "SQUARE";
const RESET = "RESET"

export {
    ADD,
    SQUARE,
    RESET
}