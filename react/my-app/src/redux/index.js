//1 import React
import React from "react";
import ReactDOM from "react-dom/client";
import { legacy_createStore as createStore } from "redux";
import math from "./reducer/math"
import { Provider } from "react-redux"
import Container from "./Components/Container"
import store from "./store/store"
//1. store = datawarehouse
//2. state = data in datawarehouse 
//3. action = object
//4. dispatch store.dispatch(action)
//5. reducer = function

const App = () => {
    return (
        <Provider store={store}>
            <Container />
        </Provider>
    )
}
const root = ReactDOM.createRoot(document.getElementById("root"));
function rendering() {
    root.render(<App />)
}
rendering();
store.subscribe(rendering)

