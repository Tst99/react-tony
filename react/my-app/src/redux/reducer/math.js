const math = (state = 10, action) =>{
    switch (action.type) {
        case 'ADD':
            return state + action.num;
        case "SQUARE":
            return state**2;
        case "RESET":
            return action.num
        default:
            return state
    }
}

export default math