import math from "../reducer/math";
import thunk from "redux-thunk";
import {applyMiddleware, legacy_createStore as createStore } from "redux";

const store = createStore(math,applyMiddleware(thunk))

export default store

