const {createProxyMiddleware} = require('http-proxy-middleware')

module.exports =function(app){
    app.use(
        '/api',
        createProxyMiddleware({
            target: 'https://i.maoyan.com',
            changeOrigin:true,
        })
    );

    app.use(
        '/graphql',
        createProxyMiddleware({
            target: 'http://localhost:8080/graphql',
            changeOrigin:true,
        })
    );
};
