// 1. Custom component

// import core module
import React, { FunctionComponent ,} from "react";
import { View ,Text,StyleSheet} from "react-native";

// create component

const App1  = () => {

    return(
        <View style={styles.container}>
            <Text style={styles.txt1}>Hello,ReactNative!</Text>
        </View>
    )
}

// sample code
const styles = StyleSheet.create({
    txt1:{
        color:"red"
    },
    container:{
        backgroundColor:"#ccc"
    }
})


// export
export default App1