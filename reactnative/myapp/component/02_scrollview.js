import React from 'react'
import { Text, View, StyleSheet, ScrollView } from 'react-native'

export default function App2() {
    return (
        <View style={styles.container}>
            <ScrollView horizontal>
                <Text style={styles.txt1}>
                    Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!Hello!!!
                </Text>
                <Text>
                    YO!
                </Text>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    txt1: {
        color: "red"
    },
    container: {
        width: 100,
        height: 100,
        backgroundColor: "pink"
    }
})