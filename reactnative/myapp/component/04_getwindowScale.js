import React from 'react'
import { Text, View, StyleSheet, ScrollView, Dimensions } from 'react-native'

const {width,height,scale} = Dimensions.get("window")


export default function App3() {
    return (
        <View style={styles.container}>
            <Text>Width of Monitor : {width}</Text>
            <Text>Height of Monitor : {height}</Text>
            <Text>Ratio of Monitor : {scale}</Text>
            <View style={styles.line}></View>
        </View>
    )
}

const styles = StyleSheet.create({ 
    line:{
        height:1/scale,
        backgroundColor:"red"
    },
    container: {
        width: width,
        height: height,
        backgroundColor: "green"
    }
})