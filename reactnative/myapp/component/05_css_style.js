import React from 'react'
import { Text, View, StyleSheet} from 'react-native'

export default function App5() {
  return (
    <View style={styles.container}>
        <View style={styles.box1}><Text style={{fontSize:20}}>1</Text></View>
        <View style={styles.box2}><Text style={{fontSize:30}}>2</Text></View>
        <View style={styles.box3}><Text style={{fontSize:50}}>3</Text></View>
    </View>
  )
}

const styles = StyleSheet.create({
    container:{
        display:"flex",
        flex:1,

        // flexDirection : "row","row-reverse","column","column-reverse"
        flexDirection:"row",

        // justifyContent: : "flex-start","flex-end","center","space-between","space-around"
        justifyContent:"space-around",

        // alignItems : "flex-start","flex-end","center","stretch","baseline"
        alignItems:"baseline"

    },
    box1:{
        width:100,
        height:100,
        backgroundColor:"#ccc"
    },
    box2:{
        width:100,
        // height:100,
        backgroundColor:"#fcf"
    },
    box3:{
        width:100,
        height:100,
        backgroundColor:"#ffc"
    }
})