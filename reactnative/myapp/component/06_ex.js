import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

let array = [1, 2, 3]

export default function App6() {
    return (
        <View>
            <Text>123456</Text>
            <View style={{flexDirection:"row"}}>
                {
                    array.map(num =>
                        <View style={styles[`box${num}`]}><Text>{num}</Text></View>
                    )
                }
            </View>
            <View style={{flexDirection:"row"}}>
                {
                    array.map(num =>
                        <View style={styles[`box${num}`]}><Text>{num}</Text></View>
                    )
                }
            </View>
            <View style={{flexDirection:"row"}}>
                {
                    array.map(num =>
                        <View style={styles[`box${num}`]}><Text>{num}</Text></View>
                    )
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({ 
    box1 : {
        width : "30%",
        borderStyle : "solid",
        backgroundColor : "red",
        borderColor : "black",
        borderWidth : 5         
    },
    box2 : {
        width :"20%",
        borderStyle : "solid",
        backgroundColor : "pink",
        borderColor : "black",
        borderWidth : 5          
    },
    box3 : {
        width : "50%",
        borderStyle : "solid",
        backgroundColor : "blue",
        borderColor : "black" ,
        borderWidth : 5         
    }
})