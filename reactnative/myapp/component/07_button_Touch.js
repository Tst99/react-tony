import React, { useEffect, useState } from 'react'
import { View ,Text,Button,TouchableOpacity, StyleSheet} from 'react-native'

export default function App7() {
    const [state,setState] = useState(0)

    useEffect (()=>{
        setState(1)
    },[])

    const handlePress = () =>{
        setState(state+1) 
        alert(state)
    }

  return (
    <View>
        <Button  title='Click Me' color="skyblue" onPress={()=>handlePress()}></Button>
        <TouchableOpacity onPress={()=>handlePress()} style={styles.btn}><Text>Click me</Text></TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
    btn:{
        width : 100,
        height : 100,
        backgroundColor: "red",
        borderRadius : 50,
        justifyContent : "center",
        alignItems : "center"
    }
})