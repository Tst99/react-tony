import React from 'react'
import { Text, View,Image ,ScrollView, StyleSheet} from 'react-native'
import IMG from "../res/1.png"

export default function App8() {
  return (
    <ScrollView>
        <Text>Method 1 :</Text>
        <Image source={IMG}
        style = {styles.img}></Image>
        <Text>Method 2 :</Text>
        <Image source={require("../res/1.png")}
        style = {styles.img}></Image>
        <Text>Method 3 :</Text>
        <Image source={{uri:"https://e1.pngegg.com/pngimages/298/527/png-clipart-snoopy.png"}}
        style = {styles.img}></Image>
        <Text>Method 4 :</Text>
        <Image source={{uri:"snoopy1"}}></Image>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
    img:{
        width:300,
        height: 100
    }
})