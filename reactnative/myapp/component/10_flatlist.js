import React, { useState } from 'react'
import { Text, View ,FlatList} from "react-native"

const arr_data = [
    {
        id: 1,
        data: 10,
    },
    {
        id: 2,
        data: 20,
    },
    {
        id: 3,
        data: 30,
    },
    {
        id: 4,
        data: 40,
    },
]

const App10 = () => {
    const [data,setdata] = useState(arr_data)

    const renderDate = ( {item}) => {
        return(
            <Text>{item.id} : {item.data}</Text>
        )
    }

    return (
        <View>
            <Text>
                lol
            </Text>
            <FlatList 
                numColumns={1} 
                data={arr_data}
                renderItem={renderDate}
            ></FlatList>
        </View> 
    )
}

export default App10