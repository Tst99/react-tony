import React, { useState } from 'react'
import { StyleSheet, View, Image, FlatList, Text } from 'react-native'

const img_data=[
    {
        id : 1,
        src: require('../res/2.jpg'),
        name:"dog1",
        des:"dooooog"
    },
    {
        id : 2,
        src: require('../res/3.jpg'),
        name:"dog2",
        des:"dooooog"
    },
    {
        id : 3,
        src: require('../res/4.jpg'),
        name:"dog3",
        des:"dooooog"
    },
    {
        id : 4,
        src: require('../res/5.jpg'),
        name:"dog4",
        des:"dooooog"
    },
    {
        id : 5,
        src: require('../res/6.jpg'),
        name:"dog5",
        des:"dooooog"
    },
]

export default function App11() {
    const [data,setdata] = useState(img_data)

    const renderItem = ({item}) =>{
        return(
            <View>
                <Image style={styles.show} source={item.src}></Image>
                <Text>{item.id} : {item.name}</Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.container1}>
                <Image style={styles.top} source={require("../res/1.png")}></Image>
            </View>
            <View>
                <FlatList
                numColumns={3}
                data={img_data}
                renderItem={renderItem}
                ></FlatList>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ccc",
    },
    container1:{
        flexDirection: "row",
        justifyContent: "space-evenly"
    },
    top: {
        width: 50,
        height: 50,
    },
    show:{
        width :120,
        height: 120
    }
})
