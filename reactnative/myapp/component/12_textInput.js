import React from 'react'
import {View, TextInput, Text ,StyleSheet} from 'react-native'

export default function App12() {
  return (
    <View>
        <TextInput style={styles.txtInput}
          placeholder="please enter..."
          placeholderTextColor="grey"
          maxLength={8}
          underlineColorAndroid="black"
          // secureTextEntry={true}
        />
        <Text>123</Text>
    </View>
  )
}

const styles =StyleSheet.create({
  txtInput:{
    borderWidth:1,
    borderColor:"#000",
    marginTop:50,
    // padding:0,
    height:40
  }
})
