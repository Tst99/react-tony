import React from 'react'
import { View, StyleSheet, Dimensions, Image, TextInput, TouchableOpacity, Text } from 'react-native'


const { width, height,scale } = Dimensions.get("window")
export default function App13() {
    return (
        <View style={styles.container}>
            <View style={styles.wrap}>
                <Image source={require("../res/1.png")} style={styles.avatar} />
                <TextInput style={[styles.txtInput, styles.username]}
                    placeholder="please enter user name"
                    placeholderTextColor="#ddd"
                    underlineColorAndroid="transparent"
                />
                <TextInput style={styles.txtInput}
                    placeholder="please enter password"
                    placeholderTextColor="#ddd"
                    underlineColorAndroid="transparent"
                />
                <TouchableOpacity style={styles.loginBtn} activeOpacity={0.5}>
                    <Text style={styles.loginTxt}>Login</Text>
                </TouchableOpacity>
                <View style={styles.btns}>
                    <TouchableOpacity activeOpacity={0.5}>
                        <Text>Forget password</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.5}>
                        <Text>New user </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.bottomBox}>
                    <View style={styles.line}></View>
                    <Text style={styles.bottomTxt}>123</Text>
                    <View style={styles.bottomImages}>
                        <TouchableOpacity activeOpacity={0.9}>
                            <Image style={styles.logo} source={require("../res/1.png")} />
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={0.9}>
                            <Image style={styles.logo} source={require("../res/1.png")} />
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={0.9}>
                            <Image style={styles.logo} source={require("../res/1.png")} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "grey"
    },
    wrap: {
        width: 320,
        height: height,
        backgroundColor: "#eee",
        alignItems: "center"
    },
    avatar: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderColor: "red",
        marginTop: 50,
        marginBottom: 30
    },
    username: {
        marginBottom: 3
    },
    txtInput: {
        width: "80%",
        backgroundColor: "#fff",
        height: 40,
        paddingLeft: 10
    },
    loginBtn: {
        width: "80%",
        height: 30,
        backgroundColor: "skyblue",
        borderRadius: 4,
        marginTop: 20
    },
    loginTxt: {
        textAlign: "center",
        lineHeight: 30,
        color: "#fff"
    },
    btns: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "80%",
        marginTop:10
    },
    line: {
        height:1/scale,
        width:"100%",
        backgroundColor:"#999"
    },
    logo: {
        width: 40,
        height: 40,
        borderRadius:20,
        backgroundColor:"black",
    },
    bottomBox:{
        width:"100%",
        alignItems:"center",
        position:"absolute",
        bottom:40,
    
    },
    bottomTxt:{
        marginTop:-8,
        backgroundColor:"#eee",
        width:150,
        textAlign:"center",
        fontSize:12
    },
    bottomImages:{
        width:"80%",
        flexDirection:"row",
        justifyContent:"space-evenly",
    }
})
